| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Principles
Do stuff for reasons.

<!-- MarkdownTOC -->

- [Remember: You Will Die](#remember-you-will-die)
- [The Glass Is Already Broken](#the-glass-is-already-broken)
- [Do Stuff For Reasons](#do-stuff-for-reasons)
- [The Universe Is Causal](#the-universe-is-causal)
- [The Universe Is Probabilistic](#the-universe-is-probabilistic)
- [I am The Sole Cause of My Misery](#i-am-the-sole-cause-of-my-misery)
- [Hope is Folly](#hope-is-folly)
- [Actions Express Priorities](#actions-express-priorities)
- [Less is More](#less-is-more)
- [The best way to stay organized is to have less stuff to organize.](#the-best-way-to-stay-organized-is-to-have-less-stuff-to-organize)
- [Explicit is Better Than Implicit](#explicit-is-better-than-implicit)
- [Time is Your Most Precious Asset](#time-is-your-most-precious-asset)
- [Prioritize Ruthlessly](#prioritize-ruthlessly)
- [Set your agenda, or it will be set for you](#set-your-agenda-or-it-will-be-set-for-you)
- [Strengthen your willpower, or you will be vulnerable to manipulation](#strengthen-your-willpower-or-you-will-be-vulnerable-to-manipulation)
- [Deserve's got nothin to do with it](#deserves-got-nothin-to-do-with-it)
- [It's All Quantum Physics](#its-all-quantum-physics)
- [Slow is Smooth, and Smooth is Fast](#slow-is-smooth-and-smooth-is-fast)
- [An ounce of prevention is worth a pound of cure](#an-ounce-of-prevention-is-worth-a-pound-of-cure)
- [WIP: Do The Work](#wip-do-the-work)
- [WIP: Discipline = Freedom](#wip-discipline--freedom)
- [WIP: Lay a Perfect Brick](#wip-lay-a-perfect-brick)
- [WIP: Just Start](#wip-just-start)

<!-- /MarkdownTOC -->

When you have principles, and you understand why you have each of them, you don't have to start from scratch every time you have to make a decision. Principles are effectively a form of *data compression* that allows us to save time when making decisions.

Some of these principles will be derived from other principles. Some of these principles will conflict with each other, which is why it is important to understand their relative priority, and how to balance them.

<a id="remember-you-will-die"></a>
## Remember: You Will Die
Synergizes with: [The Glass Is Already Broken](#the-glass-is-already-broken)

<a id="the-glass-is-already-broken"></a>
## The Glass Is Already Broken
Synergizes with: [Remember: You Will Die](#remember-you-will-die)

- Loss aversion is one of the strongest possible motivations for humans.
- People equate the emotional impact of a loss at about twice the magnitude of an equivalent gain.
- Thus, we must remember the law of impermanence: nothing lasts, everything changes.
- When you understand this and practice it, you make better decisions.

<a id="do-stuff-for-reasons"></a>
## Do Stuff For Reasons

<a id="the-universe-is-causal"></a>
## The Universe Is Causal

- The Big Bang started a single, massive, chemical chain reaction that has been playing out since, for billions of years.

<a id="the-universe-is-probabilistic"></a>
## The Universe Is Probabilistic

- Newtonian physics is completely deterministic, so if you had perfect knowledge of a purely Newtonian universe, you could predict the future.
- However, we live in a universe governed by Quantum physics, which is not deterministic.
- Thus, there exist things in the world that are impossible to predict with 100% certainty.
- However, probability, represents our ignorance. It is not a attribute of the world.
  - Example: if you flip a coin, cover the result, then guess what it is, you could say that you have a 50% chance of guessing correctly. That is not a statement about the world.
  - The state of the coin is already determined, there is either a 100% probability that it is heads, or a 100% probability that it is tails. Your ignorance does not change that.
  - Thus, there are no such things as "mysterious phenomena." The quality of being mysterious is simply a statement about our own ignorance, not an attribute of the phenomenon.

<a id="i-am-the-sole-cause-of-my-misery"></a>
## I am The Sole Cause of My Misery

<a id="hope-is-folly"></a>
## Hope is Folly

- When you find yourself hoping for an outcome, you have already lost.
- Think through, list out, and prepare for all contingencies (within reason).
- If you have prepared as much as you can, then there is no need for hope. Let go of attachment to any outcome.

<a id="actions-express-priorities"></a>
## Actions Express Priorities

<a id="less-is-more"></a>
## Less is More
Sometimes conflicts with: [Explicit is Better Than Implicit](#explicit-is-better-than-implicit)

<a id="the-best-way-to-stay-organized-is-to-have-less-stuff-to-organize"></a>
## The best way to stay organized is to have less stuff to organize.

<a id="explicit-is-better-than-implicit"></a>
## Explicit is Better Than Implicit
Sometimes conflicts with: [Less is More](#less-is-more)

<a id="time-is-your-most-precious-asset"></a>
## Time is Your Most Precious Asset
Deduced from: [Remember: You Will Die](#remember-you-will-die)

<a id="prioritize-ruthlessly"></a>
## Prioritize Ruthlessly

- aka You can have (almost) anything you want, but you can't have everything you want.
- Corollary: You can be excellent at (almost) anything, but you can't be excellent at everything.

Synergizes with: [Remember: You Will Die](#remember-you-will-die) and [Time is Your Most Precious Asset](#time-is-your-most-precious-asset)

<a id="set-your-agenda-or-it-will-be-set-for-you"></a>
## Set your agenda, or it will be set for you

- Everyone in the world has wants.
- If you are not clear on what you want, or if you are not steadfast in your pursuit of it, then someone else who is will use you to achieve their own wants.
- It's not malevolent (well, in some cases it is). It's mostly just the nature of human psychology.
- If I want something, and you can help me achieve it, I will, consciously or subconsciously, try to conscript you into my service.
- If our wants are aligned, then awesome! We can create beautiful things together.
- If your wants are not aligned with mine, then it becomes a battle of wills, and it's likely that the weaker person will bend to the stronger one.
- Again, this is not (always) malevolent, but it is a result of living in a world where people pursue things that they want.
- So, you must be clear on your wants so that you do not unconsciously fall into the service of someone who you are not aligned with.

<a id="strengthen-your-willpower-or-you-will-be-vulnerable-to-manipulation"></a>
## Strengthen your willpower, or you will be vulnerable to manipulation
Deduced from: [Set your agenda, or it will be set for you](#set-your-agenda-or-it-will-be-set-for-you)

- There are people in the world who are directly incentivized to get you to make poor decisions (e.g. the tobacco industry, the fast food industry).
- Companies have spent millions of dollars on teams of researchers, figuring out the very best ways to manipulate you into buying their products and make their products more addictive.
- If your will is weak, you will fall prey to these tactics. You're completely outgunned, which means you have to be hyper-vigilant.
- The best defense against this is understanding clearly what you want, what your principles are, and developing your strength of will.


<a id="deserves-got-nothin-to-do-with-it"></a>
## Deserve's got nothin to do with it
Synergizes with [The Universe Is Causal](#the-universe-is-causal)

- Good things happen to bad people. Bad things happen to good people.
- The Universe is neither malevolent nor beneficent. It is indifferent, cold, and uncaring.
- You are not entitled to anything. You don't get "points for effort."
- You have some agency in the universe (though probably less than you think), some power to create effects.

<a id="its-all-quantum-physics"></a>
## It's All Quantum Physics

- aka The Universe Is Flat
- Newtonian physics is a model
- Social constructs (the "intersubjective"), like money, laws, and numbers, are just collective agreements among people. They are not 'real' in the sense that they do not exist physically in the universe. They are however 'real' in the sense that they have an impact in how we interact with people.
- Everything that you see around you is made up of a finite, countable (but large), number of atoms. Each of those atoms is made up of a finite number of quarks, which we cannot see directly, but know must exist.

<a id="slow-is-smooth-and-smooth-is-fast"></a>
## Slow is Smooth, and Smooth is Fast

Synergizes with [An ounce of prevention is worth a pound of cure](#an-ounce-of-prevention-is-worth-a-pound-of-cure)

- When you rush, you are more likely to make mistakes
- Mistakes will often cost you more than doing the act slowly and correctly the first time
- This is why I don't rush through working out, I won't work out even if slightly injured.
- This is why we write tests first and do thorough code reviews.
  - Catching mistakes in pair programming is cheaper than catching them in review.
  - Catching them in review is cheaper than catching them in staging.
  - Catching them in staging is cheaper than catching them in production.

<a id="an-ounce-of-prevention-is-worth-a-pound-of-cure"></a>
## An ounce of prevention is worth a pound of cure

This is the principle behind my policy [Conquering Fears](/policies/conquering-fears.md)

- When you find yourself fearful of some bad outcome:
  - First, acknowledge that much of the universe is beyond our control, so there is a possibility that bad outcome will happen no matter what you do.
  - Then, list out the low-cost things you can do to improve your chances of preventing that bad thing from happening. Do those things.
  - Relax, there is no step three.

<a id="wip-do-the-work"></a>
## WIP: Do The Work

<a id="wip-discipline--freedom"></a>
## WIP: Discipline = Freedom

<a id="wip-lay-a-perfect-brick"></a>
## WIP: Lay a Perfect Brick

<a id="wip-just-start"></a>
## WIP: Just Start
