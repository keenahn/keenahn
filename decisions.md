| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Decisions

Decisions are examples of Principles in action.

## How to Make Decisions
A practical guide

### Guidelines
1. Solve one problem at a time.
2. When you see a cost, ask what's the benefit. When you see a benefit, ask what's the cost.
3. Understand and accept that you will **always** have imperfect information.
4. Base your decisions on [principles](principles.md).
5. Know that your first answer will most likely be wrong, so don't be afraid to revisit past decisions if you have more information.
6. Make decisions, understanding that solutions are not real. Decisions capture the direction of your evolution. Decisions are just snapshots at a given moment in time of your thinking. Solutions imply static end-states and perfect answers. Problems are real. Decisions are real. Solutions are not.

The micro informs the macro, informs the micro.

### Process
1. Write down the (one) problem you are trying to solve or question you are trying to answer.
2. Brainstorm and list possible courses of action.
3. Write out the relative costs and benefits of each these.
4. Write down the principles that support each of these.
5. If one course of action seems to be the best, choose it, and write out your next actions that result from that decision.
6. If none of the options seem satisfactory, you **may** want to spend more time investigating in order to expand the possibility space. Be careful here, as it is easy to convince yourself that you do not have enough information, when in fact you are stalling because you don't want to make a tough decision.

