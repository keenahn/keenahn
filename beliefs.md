| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Beliefs

This list is incomplete and out of date. That will be true no matter when you read it.

## Opinions
### About the world
- Factory farming is wrong. Eating sentient creatures is wrong. As a result of this belief, I will employ a mostly vegan diet.
- Killing innocent people is wrong. Therefore, the Death Penalty, in its current state where we cannot be 100% sure of the guilt of the sentenced, is wrong.
- Basic health care should be widely and freely available to people who want it, and the government should pay for it. The benefits to society will lead to our entire country being more productive, and that has a monetary value that can be captured, a percent of which should be used for healthcare.
- Abortion should be widely and freely available to women who want it without exception until fetal viability, and with exceptions after that. Birth control should be widely and freely available to anyone who wants it.
- Women should not be stigmatized for choosing abortion, one out of three women will get one by the time they're 45.

