| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Should I Push Through a Bad Day?

1. Is the day more than 75% over?
  1. If yes, then fudge it. Just take the rest of the day off.
  2. If no, keep reading.
2. Did you get your most important task done?
  1. If yes, then go ahead, take the rest of the day off.
  2. If no, keep reading.
3. Do you have any deadlines tomorrow for stuff that is not yet done?
  1. If yes, then keep working. People are counting on you.
  2. If no, then communicate to the stakeholders that you might be delayed.
