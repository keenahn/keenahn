| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Should I Get Out of Bed?

1. Are you sick?
    1. If so, then no. Keep sleeping
    2. If not, keep reading.
2. Did you get 7.5 hours of sleep last night?
    1. If yes, get up.
    2. If no, keep reading.
3. Did you get 6 hours of sleep last night?
    1. If yes, suck it up. Get out of bed and put on the coffee.
    2. If no, keep reading.
4. Do you have a meeting in the next two hours?
    1. If yes, get up. You can nap after. Other people are counting on you.
    2. If no, stay asleep for two more hours and set an alarm.

Remember: When choosing to sleep instead of getting up, you are almost always making the right decision. You are giving up two hours of your life right now, when you're miserable and not going to be operating at max capacity, for two productive hours later.
