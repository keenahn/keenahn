| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Lifting Contingency Plans

<!-- MarkdownTOC -->

- [In case of injury](#in-case-of-injury)
- [In case you are sick](#in-case-you-are-sick)
- [In case you miss the morning lift](#in-case-you-miss-the-morning-lift)
- [In case it is very crowded at the gym](#in-case-it-is-very-crowded-at-the-gym)
- [In case of plateauing](#in-case-of-plateauing)
- [In case you just don't feel like going to the gym](#in-case-you-just-dont-feel-like-going-to-the-gym)
- [In case you have a girl over who doesn't want to wake up at 5AM](#in-case-you-have-a-girl-over-who-doesnt-want-to-wake-up-at-5am)
- [In case you fall off the program for a week or more](#in-case-you-fall-off-the-program-for-a-week-or-more)

<!-- /MarkdownTOC -->


<a name="in-case-of-injury"></a>
## In case of injury

- The first priority of working out is defense. DO NOT LIFT if injured.
- It may take a week or even more to recover. Be OK with this. Your gainz will not evaporate.
- Slow is smooth and smooth is fast. Your progress will stop completely if you turn a minor injury into a major injury.
- In other words, your muscles are no good to you dead, so take care of them above all else.

<a name="in-case-you-are-sick"></a>
## In case you are sick

- In most cases, you would be wise to skip working out. Light activity like walking is OK but use your judgment.
- Your body needs all the resources it can get to fight the illness, so no need to provide extra stress.
- Again, remember that slow is smooth, and smooth is fast. Working out while sick might just prolong your sickness, which will ultimately not benefit you.

<a name="in-case-you-miss-the-morning-lift"></a>
## In case you miss the morning lift

- You are scheduled to lift every Wednesday, Friday, and Sunday at around 5:25AM. If you cannot make this time, attempt again around 13:00.
- If you cannot make that time, attempt again later at night.
- If you cannot make any time during that day, simply skip that day and attempt again the next day. This will shift the entire schedule off by one day, but that's OK.
- As much as possible, do not leave more than a three day gap between lifting days, but also don't beat yourself up if you can't.
- Focus on improving your adherence rate and not perfection.

<a name="in-case-it-is-very-crowded-at-the-gym"></a>
## In case it is very crowded at the gym

- This will usually be taken care of by going very early, say 5:25.
- If if it is crowded at the gym, and you estimate that the total wait time to use all the equipment you want to use will be 40 minutes or less, then wait. 40 minutes is about how long it takes to walk to and from the gym, so any wait time less than that is still more efficient than leaving and coming back.
- Otherwise, walk back and try again later.
- Remember to bring some audio books or ebooks on your phone to read while waiting, so you are not afraid of it.
- Also remember that there is no internet connection in the gym, so these items must be loaded on your phone ahead of time and not streamed.

<a name="in-case-of-plateauing"></a>
## In case of plateauing

- Plateauing is normal and will happen from time to time. Be OK with this.
- Your strength will vary from day to day based on a variety of factors. Listen to your body and don't attempt any movement that feels wrong.
- If you cannot lift the weight you are scheduled to lift, deload until you are at a weight where you can still do the required sets and reps.
- Next time, attempt to go up to the weight you were scheduled to lift the time before. If you are successful, then you would have only been set back one session.
- If you have experienced several plateaus in a row, or have gone through the deload cycle several times, it means one of several things.
  - Your form might be poor, so pay attention to it and get it back in shape.
  - Your diet might be poor, so you may need to consume more calories and protein.
  - You might not be getting enough rest, so pay attention to your sleep patterns to make sure you have adequate time to grow muscle.
  - You might need another program, or you might need accessory exercises that work the same muscles to bust through the plateau.

<a name="in-case-you-just-dont-feel-like-going-to-the-gym"></a>
## In case you just don't feel like going to the gym

- It hasn't happened yet, but it's possible that you might be tired one day or just plain resistant to going.
- If this is the case, commit to putting your feet on the floor by 5:05AM.
- Then, take pre-workout and commit to getting out the door by 5:10AM.
- Put on some motivating music to get pumped up.
- Then, commit to going into the gym.
- Then, commit to doing a warmup set with just the bar.
- If you have done all of the above and still don't feel like lifting, then OK. You can skip that day. Attempt again the next day.
- Chances are, you will have mustered the motivation to do a full session by that point, but if you haven't, then it is A-OK to skip.
- Don't beat yourself up if you have to skip. Remember, focus on increasing adherence instead of perfection.


<a name="in-case-you-have-a-girl-over-who-doesnt-want-to-wake-up-at-5am"></a>
## In case you have a girl over who doesn't want to wake up at 5AM

- Use your judgment, but you might decide that it is worth not breaking your routine and go to the gym anyway at 5, even though you will be operating on less sleep.
- Otherwise, follow the protocol for [In case you miss the morning lift](#in-case-you-miss-the-morning-lift).

<a name="in-case-you-fall-off-the-program-for-a-week-or-more"></a>
## In case you fall off the program for a week or more

- It's possible that for whatever reason, you might lose motivation for a longer period of time.
- It happens, it's natural, don't beat yourself up about it.
- If at all possible, try to lift at least once per calendar week, to maintain some sort of consistency.
- If that is even too much, try to do bodyweight exercises once per calendar week.
- Do not beat yourself up. Focus on increasing adherence and not perfection.
- If you have fallen off for a long time, then as soon as you get your head above water, restart the program but at a lighter load than where you were.
- Over the next year or so, you might have to restart several times. Habits take a few restarts to stick, sometimes.
- Above all, remember that you are doing this to improve your health, which improves everything else in your life. It might not seem like it at the time, but it is always worth investing in yourself, even if you have other crazy shit going on. Treat working out as you work eating, drinking, and sleeping. It's that important.



