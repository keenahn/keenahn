| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Policies

Policies are the practices that derive from [Principles](principles.md). They also act as a form of data compression in that they save us from having to re-make the same decisions.

No policy will be adhered to 100% of the time, so it is unrealistic to aim for perfection. Instead, I focus on rapid repairs, and increasing adherence.

All policies will have a time limit on them, at which point I will revisit them and update them. In some rare, extreme, cases, I will allow myself to change a policy before its expiration date if it becomes clear that it was ill-conceived. I will not allow myself to do this as a way of getting out of adhering.

- [Morning Routines](/policies/morning-routines.md)
- [Lifting Contingencies](/policies/lifting-contingencies.md)
- [Conquering Fears](/policies/conquering-fears.md)

