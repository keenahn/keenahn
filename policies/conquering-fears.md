| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Conquering Fears

<!-- MarkdownTOC -->

- [Procedure](#procedure)
- [Examples](#examples)
  - [Germaphobia](#germaphobia)

<!-- /MarkdownTOC -->

<a name="procedure"></a>
## Procedure

1. Acknowledge death.
2. Acknowledge that the bad thing I don't want to happen might happen.
3. Write out the set of low-cost things I can do to improve my chances, but don't freak out beyond that.

<a name="examples"></a>
## Examples

<a name="germaphobia"></a>
### Germaphobia

I am disgusted by the thought of eating poop. I am disgusted by being at the hospital. I'm terrified of public bathrooms.

1. **Acknowledge death.**
     All fears come down to the fear of death. In this case, there's a pretty direct link. Our caveman ancestors lived in a world without common sanitation, so dying from disease was the norm. Those of us who did not develop a healthy fear of disease would get killed off before reproducing, thus creating generations of germaphobes. So, as with everything, the first step is to remember that I will die. This is inevitable, no matter what else I do.

2. **Acknowledge that the bad thing I don't want to happen might happen.**
     In this case, I don't want to get sick. However, there's a good chance I will get sick at some point in my life, despite anything I do for prevention. So, better to go in with eyes open. I will get sick, eventually, and a lot of it is beyond my control.

3. **Write out the set of low-cost things I can do to improve my chances, but don't freak out beyond that.**
     When does a fear become an irrational fear? In my opinion, it does so when you start giving undue weight to that fear, or when that fear is causing you to change your behavior in a way that is disproportionate to the threat.

     In the case of germaphobia, there are definitely some real things to be afraid of. I would never advise someone to take unnecessary risks. It's all about ROI. The ROI of your preventative action should be positive compared to the threat.

     When it comes to day to day germaphobia, don't freak out about how disgusting the public bathroom (or even cleaning your own bathroom) seems. Just:

         - wash your hands thoroughly
         - don't touch your eyes or mouth
         - and wash your hands thoroughly again before eating.

     Just like that, you will greatly improve your chances, and the cost is low. Beyond that, relax. Anything else you can do (carrying anti-bacterial gel, for example) probably has a negative ROI in terms of actual effectiveness.
