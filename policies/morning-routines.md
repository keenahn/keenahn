| [Principles](/principles.md) | [Decisions](/decisions.md) | [Policies](/policies) | [Beliefs](/beliefs.md) |
|------------------------------|----------------------------|-----------------------|------------------------|

# Morning Routines

<!-- MarkdownTOC -->

- [Normal](#normal)
- [Quick Walk Before Work](#quick-walk-before-work)
- [Work Before Walk](#work-before-walk)
- [Sick or No Sleep](#sick-or-no-sleep)

<!-- /MarkdownTOC -->

<a name="normal"></a>
# Normal

| When  | What                                                                          | Dur.  |
| ----  | ----------------------------------------------------------------------------- | ----- |
| 5:00  | Wake up, make bed, download podcast, get water, pee                           | 0:05  |
| 5:05  | Out the door, start walking                                                   | 0:20  |
| 5:25  | Lift, bro (or walk back if it's a rest day)                                   | 1:00  |
| 6:25  | Walk home                                                                     | 0:20  |
| 6:45  | Back home, shower                                                             | 0:15  |
| 7:00  | Make Soylent                                                                  | 0:10  |
| 7:10  | Plan day and review previous day                                              | 0:10  |
| 7:20  | Type up timetracking                                                          | 0:10  |
| 7:30  | Write MP                                                                      | 1:00  |
| 8:30  | Update habits, food log, and MPs left sheets. Choose nonwork task from Trello | 0:10  |
| 8:40  | Slack time for work, fap, poop                                                | 1:20  |
| 10:00 | All-hands meeting + eng meeting                                               | 1:30  |
| 11:30 | The rest of the day!                                                          | 9:30  |
| 21:00 | Sleep                                                                         | 8:00  |

<a name="quick-walk-before-work"></a>
# Quick Walk Before Work

| When  | What                                                                          | Dur. |
| ----  | ----------------------------------------------------------------------------- | ---- |
| 5:00  | Wake up, make bed, download podcast, get water, pee                           | 0:05 |
| 5:05  | Out the door, start walking                                                   | 1:00 |
| 6:05  | Back home, shower                                                             | 0:15 |
| 6:20  | Make Soylent                                                                  | 0:10 |
| 6:30  | Plan day and review previous day                                              | 0:10 |
| 6:40  | Work                                                                          | 3:20 |
| 10:00 | All-hands meeting + eng meeting                                               | 1:30 |
| 11:30 | Type up timetracking                                                          | 0:10 |
| 11:40 | Write MP                                                                      | 1:00 |
| 12:40 | Update habits, food log, and MPs left sheets. Choose nonwork task from Trello | 0:10 |
| 12:50 | The rest of the day!                                                          | 8:10 |
| 21:00 | Sleep                                                                         | 8:00 |

<a name="work-before-walk"></a>
# Work Before Walk

| When  | What                                                                          | Dur. |
| ----  | ----------------------------------------------------------------------------- | ---- |
| 5:00  | Wake up, make bed, download podcast, get water, pee                           | 0:05 |
| 5:05  | Make Soylent                                                                  | 0:10 |
| 5:15  | Plan day and review previous day                                              | 0:10 |
| 5:25  | Work                                                                          | 4:35 |
| 10:00 | All-hands meeting + eng meeting                                               | 1:30 |
| 11:30 | Out the door, start walking                                                   | 0:20 |
| 11:50 | Lift, bro (or keep walking on rest days)                                      | 0:40 |
| 12:30 | Walk home                                                                     | 0:20 |
| 12:50 | Back home, shower                                                             | 0:15 |
| 13:05 | Type up timetracking                                                          | 0:10 |
| 13:15 | Write MP                                                                      | 1:00 |
| 14:15 | Update habits, food log, and MPs left sheets. Choose nonwork task from Trello | 0:10 |
| 14:25 | The rest of the day!                                                          | 6:35 |
| 21:00 | Sleep                                                                         | 8:00 |

<a name="sick-or-no-sleep"></a>
# Sick or No Sleep

| When  | What                                                                          | Dur. |
| ----  | ----------------------------------------------------------------------------- | ---- |
| 5:00  | Sleep                                                                         | 4:35 |
| 9:35  | Wake up, make bed, download podcast, get water, pee                           | 0:05 |
| 9:40  | Make Soylent                                                                  | 0:10 |
| 9:50  | Plan day and review previous day                                              | 0:10 |
| 10:00 | All-hands meeting + eng meeting                                               | 1:30 |
| 11:30 | Out the door, start walking                                                   | 0:20 |
| 11:50 | Lift, bro (or keep walking on rest days)                                      | 0:40 |
| 12:30 | Walk home                                                                     | 0:20 |
| 12:50 | Back home, shower                                                             | 0:15 |
| 13:05 | Type up timetracking                                                          | 0:10 |
| 13:15 | Write MP                                                                      | 1:00 |
| 14:15 | Update habits, food log, and MPs left sheets. Choose nonwork task from Trello | 0:10 |
| 14:25 | The rest of the day!                                                          | 6:35 |
| 21:00 | Sleep                                                                         | 8:00 |
