
A good company is an end itself
Accept things as they are, not how you want them to be
Action precedes motivation
Actions express priorities
Appreciate this moment
Be unreasonable (sometimes)
Build character not reputation
Cautious, yet bold
Cavemen didn't have mirrors
Clutter is unmade decisions
Communication is your single most important skill
Consistency over intensity
Constantly making mistakes, never repeating them
Creativity is a habit
Defeat your biases
Defer gratification
Develop an endless hunger for the truth
Develop the asker mindset
Disavowal is an incomplete strategy
Discipline = freedom
Do epic shit
Do stuff for reasons
Don't expect others to change
Don't half-ass anything important
Don't optimize prematurely
Don't scale prematurely
Don't throw good money after bad
Effectiveness over efficiency
Embrace failure
Enjoy the process
Everything seems impossible until you do it
Excuses often look like very good reasons
Execution is a multiplier on ideas
Exploit the highs, ride out the lows
Fear is the mind killer
Find a way or make one
Fix the macro before optimizing the micro
Genius is patience
Give a shit
Give without expectation
Hope is folly
Hypotheses are useless if they aren't testable
I am the sole cause of my misery
I choose growth over comfort
If you're going through hell, keep going
Invest in tools that you use the most
It's a cold and uncaring universe we live in (but it's the only one we've got)
It's you versus you
Just because I'm a hypocrite doesn't mean I'm wrong
Just because I'm explaining something doesn't mean I'm advocating it
Just start
Kill your darlings
Life is too short for embarrassment
Life isn't fair
Live deliberately
Live in reality
Make a difference
Make tough decisions
Never mistake kindness for weakness
No Mo FOMO
No purpose without action, no action without purpose
Notice excuses
Passion is not enough
Pivoting is a verb, not a state
Play to win
Potential is an abstract concept
Practice saying no
Prioritize ruthlessly
Procrastination = resistance = excuses = unmade decisions
Productivity is not an end itself
Real artists ship
Red, Green, Refactor
Remember the arrival fallacy
Remember: You will die
Roll with the punches
Run toward fires
Spend every moment intentionally
Stay hungry, stay humble
Stop discounting the past and being super optimistic about the future
Stop rationalizing bad behavior
Stop sleepwalking
Strength through adversity
Strive for an outcome but don't get attached to it
Strive to lead a life without bullshit
Sucking at something is the first step to being sort of good at it
Switch lanes instead of braking
The glass is already broken
The journey is the destination
The master has failed more times than the novice has even tried.
The only way out is through
The perfect time is never
The responsible party is whomever must bear the consequences
The tools and systems are not as important as your commitment to them
The unexamined life is not worth living
The universe is flat
The world is meant to be hacked
Think bigger
Think critically
Time = money
Time is your most precious resource
Train your dragon
Trust your team
Understand how others perceive you
We're all cognitive misers
We're all janitors, we're all CEOs
What Would Mr. Rogers Do?
When you're 90% done, you're 50% done
Worry = distraction
You attract what you are (not what you want)
You can have almost anything you want, but you can't have everything you want
You can't improve what you can't measure
You get paid on Friday, then we're even
You're either growing or you're dying
You're nothing without marketing
